/**
 * @author Miklos Magyari (miklos.magyari@@sigmatechnology.se)
 * @author Adam Knapp (adam.knapp@@ericsson.com)
 * @desc Demo module containing code decorated with document comments.
 * <p> Like
 * <ul>
 * <li>Constants</li>
 * <li>Type definitions</li>
 * </ul>
 * @since 1.0
 * @url http://internal.ericsson.com
 */
module LibWithDocComments {
	type port PT message { inout integer; } with { extension "internal" }
	
	type component CT {
		port PT pt_PT;
	}
	
	const integer libConst := 1924;
	const charstring myc := "a"; 
	const integer alfa := 0; 
	
  	/**
  	 * @param first This is the first param
  	 * @param second The second param
  	 * @param third Surprisingly the third param
  	 * @author Miklos Magyari (miklos.magyari@@sigmatechnology.se)
  	 * @desc Returns value of PI regardless of parameters 
  	 * @since 2.0
     * @status deprecated
  	 * @return Something valuable
  	*/
  	function helper(in integer first, inout integer second, out charstring third) return float {
  		return 3.14;
  	}
	
	function f2() {
		var integer x :=     alfa;  
		var charstring y;
		
		var float z := helper(1, x, y);
	}
	
	/**
	 * @desc Random text
	 * @member create
	 */
	type class innerClass { }
	
	/**
	 * @desc
	 * @member f1
	 * @member myConst
	 * @member ic
	 * @member create
	 * @member getMyInteger
	 */
	type class BaseClass {
		private var integer f1;
		const integer myConst;
		private var innerClass ic;
		
		/**
		 * @desc Teszt
		 * @param p1
		 * @return
		 */
		create(integer p1) { 
	       myConst := 0;
	       
		}

		/**
		 * @desc
		 * @param x Rojtokmuzsaj
		 * @param y Parassapuszta
		 * @return
		 */
		public function getMyInteger(integer x, charstring y) return integer {
			return f1;
		}
		
		
	}
	
	type union MyUnion {
		integer x1,
		charstring c2
	}
	
	type record MyRecord {
	 	integer x1,
		charstring c2
	}
	
	
	type enumerated WorkDays { Monday, Tuesday, Wednesday, Thursday, Friday }
	
	
	function f_getWorkDays(
		in charstring pl_workday,
		in integer pl_int)
	return WorkDays
	{
	  return Monday;
	}
} 
